<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class Common {
   
    var $CI;
   
    function __construct($url = '')
    {
      // Copy an instance of CI so we can use the entire framework.
      $this->CI =& get_instance();
      //start native session
      session_start();
      if(!isset($_SESSION['access']))
      {
        $_SESSION['access'] = false;
        $_SESSION['error'] = array();
      }
    }
    
    /*
    * Set the user session and login
    * @param $data array (user data)
    * @return bool (true/false)
    */
    public function set_session($user_data)
    {
        if(count($user_data)>0){
            $_SESSION['access'] = true;
            $_SESSION['data'] = $user_data;
            return true;
        }else{
            return false;
        }
    }
    
    /*
    * Logout user simple function
    */
    public function logout()
    {
        $_SESSION['access'] = false;
        $_SESSION['data'] = array();
    }
    
    /*
    * Session error handling!
    * @param $bool bool (true/false)
    * @param $val text (submitted error!)
    */
    public function set_error($bool,$val)
    {
        $_SESSION['error']= array($bool,$val);
    }
    
    /*
    * Logout user simple function
    */
    public function clear_error()
    {
        $_SESSION['error'] = array();
    }
    
}