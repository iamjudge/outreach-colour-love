<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class AdminPrizes extends CI_Model {

	public function __construct() {
		$this->load->database();
	}
    
    /** This function show all prizes **/
    public function getPrizes(){
        $this->db->from('promotions_prizes');
        $query = $this->db->get();
        return $query->result_array();
	}
	
	/** this facilitates the insertion of the coupons **/
	public function insertPrize(){
		$data = array(
			'id' => '',
			'prizeName' => $this->input->post('pName'),
			'description' => $this->input->post('description'),
			'code'	=> $this->input->post('code'),
			'prizeLevel' => $this->input->post('prize_level'),
			'shippingDetails'	=> $this->input->post('shipping_details'),
			'status' => $this->input->post('status'),
			'stock' => $this->input->post('stock')
		);
		$this->db->insert('promotions_prizes',$data); 
	}
	
	/** 
		gets all details on specific item
		parameter - itemid - id of the thing
	**/
	public function getPDetails($itemID){
		$this->db->select('*');
        $this->db->from('promotions_prizes');
        $this->db->where('id', $itemID);
        $query = $this->db->get();
        return $query->result_array();
	}
	
	/**
		function in charge of updating coupon and prize
		parameter - $id -> id of item - $type -> if coupon or prize
	**/
	public function updateItem($id,$type){
		if($type==1){
			$data = array(
				'id' => $this->input->post('id'),
				'prizeName' => $this->input->post('prizeName'),
				'description' => $this->input->post('description'),
				'code'	=> $this->input->post('code'),
				'prizeLevel' => $this->input->post('prize_level'),
				'shippingDetails'	=> $this->input->post('shipping_details'),
				'status' => $this->input->post('status'),
				'stock' => $this->input->post('stock')
			);
			$this->db->where('id', $id);
			$this->db->update('promotions_prizes', $data);
		} elseif($type==2) {
			$data = array(
				'id' => $this->input->post('id'),
				'code' => $this->input->post('coupon_code'),
				'description' => $this->input->post('coupon_description'),
				'level'	=> $this->input->post('level'),
				'prize' => $this->input->post('prize'),
				'frequencyRate'	=> $this->input->post('frequency_rate'),
				'frequencyTimeNum' => $this->input->post('frequency_num'),
				'frequencyTimeRate' => $this->input->post('frequency_time_rate'),
				'expiration' => $this->input->post('expiration'),
				'status' => $this->input->post('status'),
				'restrictions' => $this->input->post('restrictions')
			);
			$this->db->where('id', $id);
			$this->db->update('promotions_coupons', $data);
		}
	}
	
	/**
		function in charge of deleting coupon and prize
		parameter - $id -> id of item - $type -> if coupon or prize
	**/
	public function deleteItem($id,$type){
		if($type==1){
			$this->db->where('id', $id);
			$this->db->delete('promotions_prizes');
		} elseif($type==2) {
			$this->db->where('id', $id);
			$this->db->delete('promotions_coupons');
		}
	}
}

?>