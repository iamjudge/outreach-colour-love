<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class AdminCoupons extends CI_Model {

	public function __construct() {
		$this->load->database();
	}
    
    /** This function show all coupons **/
    public function getCoupons(){
        $this->db->from('promotions_coupons');
        $query = $this->db->get();
        return $query->result_array();
	}
	
	/** this facilitates the insertion of the coupons **/
	public function insertCoupon(){
		$data = array(
			'id' => '',
			'code' => $this->input->post('cCode'),
			'description' => $this->input->post('cDescription'),
			'level'	=> $this->input->post('level'),
			'prize' => $this->input->post('prize'),
			'frequencyRate'	=> $this->input->post('frequency_rate'),
			'frequencyTimeNum' => $this->input->post('frequency_num'),
			'frequencyTimeRate' => $this->input->post('frequency_time_rate'),
			'expiration' => $this->input->post('expiration'),
			'status' => $this->input->post('status'),
			'restrictions' => $this->input->post('restriction')
		);
		$this->db->insert('promotions_coupons',$data); 
	}
	
	/** 
		gets all details on specific item
		parameter - itemid - id of the thing
	**/
	public function getCDetails($itemID){
		$this->db->select('*');
        $this->db->from('promotions_coupons');
        $this->db->where('id', $itemID);
        $query = $this->db->get();
        return $query->result_array();
	}
	
	/** 
		checks if coupon code is already added 
		parameter - $checker -> coupon code for checking
	**/
	public function check_coupon($checker)
	{
		$this->db->select('id');
        $this->db->from('promotions_coupons');
        $this->db->where('code', $checker);
        $query = $this->db->get();
        return $query->result_array();
	}
}

?>