<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Logs extends CI_Model {

	public function __construct() {
		$this->load->database();
	}
    
    /*
    * Ger all logs
    */
    public function getLogs(){
        $this->db->from('logs');
        $this->db->order_by("dateadded", "desc"); 
        $query = $this->db->get();        
        return $query->result_array();
	}
    
    /*
    * Ger all user logs
    */
    public function getUserLogs($userid){
        $this->db->from('logs');
        $this->db->where('user_id', $userid);
        $this->db->order_by("dateadded", "desc"); 
        $query = $this->db->get();
        return $query->result_array();
	}
        
    /*
    * Insert log
    */
    public function insert($data){
        $this->db->insert('logs',$data); 
	}
}

?>