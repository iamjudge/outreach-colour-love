<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class CcManager extends CI_Model {

	public function __construct() {
		$this->load->database();
	}
    
    /** This function show all countries **/
    public function getCountries(){
        $this->db->from('promotions_countries');
        $query = $this->db->get();
        return $query->result_array();
	}
    
    /** This function show all cities **/
    public function getCities(){
        $this->db->from('promotions_cities');
        $query = $this->db->get();
        return $query->result_array();
	}
	
	/** this facilitates the insertion of the city or country **/
	public function insertCC($type){
		if($type == "city"){
			$data = array(
				'id' => '',
				'city' => $this->input->post('city'),
				'country' => $this->input->post('country')
			);
			$this->db->insert('promotions_cities',$data); 
		} elseif($type == "country") {	
			$data = array(
				'id' => '',
				'country' => $this->input->post('country')
			);
			$this->db->insert('promotions_countries',$data); 
		}
	}
	
	/** gets the name of a country using its id **/
	public function getSpecificCountry($country){
		$this->db->select('country');
        $this->db->from('promotions_countries');
        $this->db->where('id', $country);
        $query = $this->db->get();
        return $query->result_array();	
	}
}

?>