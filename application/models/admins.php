<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Admins extends CI_Model {

	public function __construct() {
		$this->load->database();
	}
    
    /*
    * This function user in login
    */
    public function login($username,$password){
        $this->db->from('admins');
        $this->db->where('username', $username);
        $this->db->where('password', $password);
        $this->db->where('status', 1); 
        $query = $this->db->get();
        return $query->result_array();
	}
    
    /*
    * This function show specific users details
    */
    public function getAdmins(){
        $this->db->from('admins');
        $query = $this->db->get();
        return $query->result_array();
	}
    
    /*
    * Get username
    */
    public function getUsername($id){
        $this->db->from('admins');
        $this->db->where('id', $id); 
        $query = $this->db->get();
        $result = $query->result_array();
        return $result[0]['username'];
	}
    
    /*
    * This function show specific users details
    */
    public function getDetails($id){
        $this->db->from('admins');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result[0];
	}
    
    /*
    * Insert user
    */
    public function insert($data){
        $this->db->insert('admins',$data); 
	}
    
    /*
    * Update user
    */
    public function update($id,$data)
    {
        $this->db->where('id', $id);
	    $this->db->update('admins', $data);
	}
    
}

?>