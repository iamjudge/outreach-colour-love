<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->library(array('template','email','session','common'));
        $this->load->model(array('admins'));       
	}
	
	public function index(){
        //check if user is logged!
        if($_SESSION['access'] != true)redirect('/admin/login', 'refresh');
		$data = "";
		$this->template->write('title', 'Administrator');
		$this->template->set_template('admin');
		$this->template->write_view('header', 'templates/admin_header');
		$this->template->write_view('mainnav', 'templates/admin_menu');
		$this->template->write_view('content', 'admin/main', $data);
		$this->template->render();
	}
	
	public function login()
    {
        //check if user is logged!
        if($_SESSION['access'] == true)redirect('/admin/index', 'refresh');
        //added
		$data = "";
        if($_POST):        
            $username = $this->input->post('username');
            $password = md5($this->input->post('password'));
            if($username && $password){
                $userdata = $this->admins->login($username,$password);
                if(count($userdata)>0){
                    $set = $this->common->set_session($userdata);
                    $this->common->set_error(true,"Welcome back!");
                    redirect('/admin/index', 'refresh');
                }else{
                    $this->common->set_error(false,"Invalid Credentials!");
                }
            }else{
                $this->common->set_error(false,"PLease provide username / password!");
            }
         endif;
		$this->template->write('title', 'Administrator login');
		$this->template->set_template('admin_login');
		$this->template->write_view('header', 'templates/admin_header');
		$this->template->write_view('mainnav', 'templates/admin_menu');
		$this->template->write_view('content', 'admin/login', $data);
		$this->template->render();
    }

    public function logout()
    {
        $this->common->logout();
        $this->common->set_error(false,"You have successfully logout!");
        redirect('/admin/login', 'refresh');
    }
    
    public function admins()
	{
        if($_GET):
            $type = $this->input->get('type');
            $adminid = $this->input->get('adminid');
            $status = $this->input->get('status');
            if($type=="edit"){
                $data['admininfo'] = $this->admins->getDetails($adminid);
            }elseif($type=="update"){
                $adminArr = array('status' => $status);
                $this->admins->update($adminid,$adminArr);
                $this->common->set_error(true,"Profile status successfully updated.");
                redirect('/admin/admins', 'refresh');
            }else{}
        endif;
        if($_POST):
            $type = $this->input->post('type');
            $adminid = $this->input->post('adminid');
            $name = $this->input->post('name');
            $username = $this->input->post('username');
            $password = md5($this->input->post('password'));
            $email = $this->input->post('email');
            $status = $this->input->post('status');
            if($type=="update"){
                if($name && $username && $email){
                    $adminArr = array(
                        'name' => $name,
                        'username' => $username,
                        'email' => $email,
                        'phone' => $phone,
                        'status' => $status
                        );
                    $this->admins->update($adminid,$adminArr);
                    $this->common->set_error(true,"{$username}'s profile successfully updated.");
                }else{
                    $this->common->set_error(false,"An error occured, try again!");
                }
            }else{
                if($name && $username && $password && $email){
                    $adminArr = array(
                        'name' => $name,
                        'username' => $username,
                        'password' => $password,
                        'email' => $email,
                        'status' => $status
                        );
                    $this->admins->insert($adminArr);
                    $this->common->set_error(true,"You have successfully added new agent.");
                }else{
                    $this->common->set_error(false,"An error occured, try again!");
                }
            }
        endif;
        
        $data['admins']=$this->admins->getAdmins();
		$this->template->write('title', 'Manage Admins');
		$this->template->set_template('admin');
		$this->template->write_view('header', 'templates/admin_header');
		$this->template->write_view('mainnav', 'templates/admin_menu');
		$this->template->write_view('content', 'admin/manage', $data);
		$this->template->render();
	}
	
	public function coupons(){
		$data = "";
		$this->template->write('title', 'Coupons');
		$this->template->set_template('admin');
		$this->template->write_view('header', 'templates/admin_header');
		$this->template->write_view('mainnav', 'templates/admin_menu');
		$this->template->write_view('content', 'admin/view_coupons', $data);
		$this->template->render();
	}
	
	public function prizes(){
		$data = "";
		$this->template->write('title', 'Promotions Prizes');
		$this->template->set_template('admin');
		$this->template->write_view('header', 'templates/admin_header');
		$this->template->write_view('mainnav', 'templates/admin_menu');
		$this->template->write_view('content', 'admin/view_prizes', $data);
		$this->template->render();
	}
	
	public function users(){
		$data = "";
		$this->template->write('title', 'Promotions Prizes');
		$this->template->set_template('admin');
		$this->template->write_view('header', 'templates/admin_header');
		$this->template->write_view('mainnav', 'templates/admin_menu');
		$this->template->write_view('content', 'admin/view_users', $data);
		$this->template->render();
	}
}

?>