<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct() {
		parent::__construct();
        //check if user is logged!
        //if($_SESSION['access'] == true)redirect('/home/index', 'refresh');
        //$this->load->library(array('fb_ignited'));
        //$this->load->model(array('users'));
        //$this->load->helper(array('params_helper'));
        //check if login
        // Try to get the user details or catch the exception.
        try {
            $this->fb_me = $this->fb_ignited->fb_get_me();
        } catch (FBIgnitedException $e) {
            echo "There was an error trying to get your facebook details, try reloading page to try again.";
        }
        //  You can then check the status, if it hasn't already redirected.
        if ($this->fb_me) {
            echo "Welcome back, {$this->fb_me['first_name']}!";
        } else {
            echo "Welcome, Guest! Please login";
        }
	}
     
	public function index()
	{
        print "<br/>";
        if ($this->fb_me) {
            $logout_url = $this->fb_ignited->fb_logout_url();
            print "<a href='{$logout_url}'>logout</a>";
        } else {
            $login_url = $this->fb_ignited->fb_login_url();
            print "<a href='{$login_url}'>login</a>";
        }
        
        print "<br/>";
        //print $this->fb_ignited->fb_login_url(array('script'=>true, 'scope'=>'email', 'redirect'=>'http://promotions.colourlove.com/login/index/'));
	}
    
    public function processing()
    {
        print "processing..";
    }
}
