<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

    public function __construct() {
		parent::__construct();
        //check user's ip
        $this->load->helper(array('countries'));
        $this->load->library('facebook');
	}
    
    public function index()
    {   
        // See if there is a user from a cookie
        $user = $this->facebook->getUser();
        $data['appID'] = $this->facebook->getAppID();
        $data['user'] = $user;
        if ($user) {
          try {
            // Proceed knowing you have a logged in user who's authenticated.
            $data['user_profile'] = $this->facebook->api('/me');
            $data['list'] = $this->fb_list_friends("uid, name, username"); 
          } catch (FacebookApiException $e) {
            //echo '<pre>'.htmlspecialchars(print_r($e, true)).'</pre>';
            $data['user'] = null;
          }
        }
        $this->load->view('facebook',$data);
    }
    
    public function setstatus()
    {
        $points = (int)$this->input->post('mypoints');
        // See if there is a user from a cookie
        $user = $this->facebook->getUser();
        if ($user) {
          try {
            if($points > 0):
                $message = 'Hurry! I win '.$points.' points. Try your luck now at @ http://promotions.colourlove.com/';
            else:
                $message = 'Oh! I just got consulation price. Try your luck now at @ http://promotions.colourlove.com/';
            endif;
            $statusUpdate = $this->facebook->api('/me/feed', 'post',
                array('name'=>'Colourlove Win a Price Daily','message'=> $message,
                'privacy'=> array('value'=>'CUSTOM','friends'=>'SELF'),
                'description'=>'The Craziest Paint 5K & Festival in the World! Sign up and go wild.',
                'picture'=>'http://promotions.colourlove.com/assets/safe_image.jpg',
                'caption'=>'colourlove5k','link'=>'http://promotions.colourlove.com/'));            
          } catch (FacebookApiException $e) {
            print "error";
          }
        }
    }
    
    public function game()
    {
        $data = array();
        $this->load->view('game',$data);
    }
    
	public function facebook()
	{
        $IPaddress=$_SERVER['REMOTE_ADDR']; 
        $two_letter_country_code=$this->iptocountry($IPaddress);
        $countries = countries();
        
        $three_letter_country_code= $countries[ $two_letter_country_code][0];
        $country_name=$countries[$two_letter_country_code][1]; 
        
        $data['country'] = "IP Address: $IPaddress<br>";
        $data['country'] = "Two letters code: $two_letter_country_code<br>";
        $data['country'] .= "Three letters code: $three_letter_country_code<br>";
        $data['country'] .= "Country name: $country_name<br>";
        
        // To display flag
        $file_to_check = "assets/flags/".$two_letter_country_code.".gif";
        
        if (file_exists($file_to_check)){
            $data['country'] .= "<img src=$file_to_check width=30 height=15><br>";
        }else{
            $data['country'] .= "<img src=assets/flags/noflag.gif width=30 height=15><br>";
        }
        
        //facebook
        $user = $this->facebook->getUser();
        $data['appID'] = $this->facebook->getAppID();
        
        if ($user) {
            try {
                $data['user_profile'] = $this->facebook->api('/me');
            } catch (FacebookApiException $e) {
                $user = null;
            }
        }else {
            $this->facebook->destroySession();
        }

        if ($user) {

            $data['logout_url'] = base_url('welcome/logout'); // Logs off application
            // OR 
            // Logs off FB!
            // $data['logout_url'] = $this->facebook->getLogoutUrl();
            
            //print my details
            $me = $this->facebook->api('/me');
            $data['userinfo'] = '<div><img src="https://graph.facebook.com/'.$me['username'].'/picture"><br/>';
            $data['userinfo'] .= $me['name'];
            $data['userinfo'] .= "</div>";
            //get userlist
            $data['userinfo'] .= "<strong>List of my friends who also use colourlove5k application.</strong> <br/>";
            $list = $this->fb_list_friends("uid, name, username"); 
            foreach($list as $u):
                $data['userinfo'] .= '<div style="width:100px; float:left;"><img src="https://graph.facebook.com/'.$u['username'].'/picture"><br/>';
                $data['userinfo'] .= $u['name'];
                $data['userinfo'] .= "</div>";
            endforeach;        
            $data['userinfo'] .= "<div style='clear:both;'></div>";
            $data['userinfo'] .= "<br/>";
            $data['userinfo'] .= "<strong>I like Colourlove5k facebook fan page? </strong>";
            $like = $this->fb_is_liked($user,"132739253601998");
            $data['userinfo'] .=  $like?"YES":"NO";
            $data['userinfo'] .= "<br/>";
            $data['userinfo'] .= '<br/><br/><iframe src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcolourlove5k&amp;width&amp;layout=button_count&amp;action=like&amp;show_faces=true&amp;share=true&amp;height=21&amp;appId=1425521834334444" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:21px;" allowTransparency="true"></iframe>';
            //post status :) 
            /*          
           try {
           $statusUpdate = $this->facebook->api('/me/feed', 'post',
                     array('name'=>'Colourlove on Facebook','message'=> 'Get excited! The wackiest colour run is on its way. Are you ready?',
                     'privacy'=> array('value'=>'CUSTOM','friends'=>'SELF'),
                     'description'=>'The Craziest Paint 5K & Festival in the World! Sign up and go wild.',
                     'picture'=>'http://promotions.colourlove.com/assets/safe_image.jpg',
                     'caption'=>'colourlove5k','link'=>'apps.facebook.com/colourlove5k'));
            } catch (FacebookApiException $e) {
                print $e;
            }
            */
            /* other version!
            $attachment = array(
                'message' => 'this is my message',
                'name' => 'This is my demo Facebook application!',
                'caption' => "Caption of the Post",
                'link' => 'http://mylink.com',
                'description' => 'this is a description',
                'picture' => 'http://mysite.com/pic.gif',
                'actions' => array(
                    array(
                        'name' => 'Get Search',
                        'link' => 'http://www.google.com'
                    )
                )
            );
            $result = $facebook->api('/me/feed/', 'post', $attachment);
            */
            

            // OR 
            // Logs off FB!
            // $data['logout_url'] = $this->facebook->getLogoutUrl();

        } else {
            $data['login_url'] = $this->facebook->getLoginUrl(array(
                'redirect_uri' => base_url('/welcome'),
                'scope' => array(
                                //'user_about_me',
                                //'user_activities',
                                //'user_birthday',
                                //'user_checkins',
                                //'user_education_history',
                                //'user_events',
                                //'user_groups',
                                //'user_hometown',
                                //'user_interests',
                                //'user_likes',
                                //'user_location',
                                //'user_notes',
                                //'user_online_presence',
                                //'user_photo_video_tags',
                                //'user_photos',
                                //'user_relationships',
                                //'user_relationship_details',
                                //'user_religion_politics',
                                //'user_status',
                                //'user_videos',
                                //'user_website',
                                //'user_work_history',
                                'email',
                                'read_friendlists',
                                //'read_insights',
                                //'read_mailbox',
                                'read_requests',
                                'read_stream',
                                //'xmpp_login',
                                //'ads_management',
                                //'create_event',
                                //'manage_friendlists',
                                //'manage_notifications',
                                'offline_access',
                                //'publish_checkins',
                                'publish_stream',
                                //'rsvp_event',
                                //'sms',
                                //'manage_pages',
                                'publish_actions') // permissions here
            ));
        }
        
		$this->load->view('welcome_message',$data);
	}
    
    /**
     * Determines if the user has liked the app
     *
     * This method will check to see if the users have liked the application
     *
     * @return boolean
     * @throws FBIgnitedException
     */
     
    private function fb_is_liked($user,$pageid)
    {
        try {
            $request = $this->facebook->api("/{$user}/likes/{$pageid}");
        } catch (FacebookApiException $e) {
            print $e->getMessage();
        }
        if ($request['data'] || $request->data) {
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * Determines if the user bookmarked the app
     *
     * This method will check to see if the users have bookmarked the application
     *
     * @return boolean
     * @throws FBIgnitedException
     */
    private function fb_is_bookmarked()
    {
        try {
            $datas = $this->fb_fql('SELECT bookmarked FROM permissions WHERE uid = me()');
        } catch (FBIgnitedException $e) {
            print $e->getMessage();
        }
        if ($datas) {
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * Posts a notification to users
     *
     * This method pushes a message to
     *
     * @param string  $message the message that will be pushed to the user
     * @param boolean $user_id the user id that we are pushing the notification to
     *
     * @return mixed
     */
    private function fb_notification($message, $user_id = null, $url)
    {
        $data = array(
                 'href'         => $url,
                 'access_token' => $this->facebook->getAccessToken(),
                 'template'     => $message,
                );
        try {
            $send_result = $this->facebook->api("/$user_id/notifications", 'post', $data);
        } catch (FacebookApiException $e) {
            print $e->getMessage();
        }

        return $send_result;
    }
    
    /**
     * Returns a list of the user's friends
     *
     * This method will return either the user's full list of friends or
     * just the users also using the application
     *
     * @param string $value determines what will be selected from users
     * @param string $list  dermines full friends list or app friends only
     *
     * @return mixed
     */
    private function fb_list_friends($value = 'uid', $list = '')
    {
        if ($list == 'full') {
            $fquery = "SELECT {$value} FROM user WHERE uid IN (SELECT uid2 FROM friend WHERE uid1 = me())";
        } else {
            $fquery = "SELECT {$value} FROM user WHERE uid IN (SELECT uid2 FROM friend WHERE uid1 = me()) AND is_app_user = 'true'";
        }

        try {
            $friends = $this->fb_fql($fquery);
        } catch (FBIgnitedException $e) {
            print $e->getMessage();
        }

        return $friends;
    }
    
    /**
     * Handles Facebook Queries
     *
     * This function is a wrapper for fql it creates a method call to api
     *
     * @param mixed $fqlquery Query to be sent to Facebook.
     *
     * @return mixed
     */
    private function fb_fql($fqlquery)
    {
        if (is_array($fqlquery)) {
            $fqlquery = json_encode($fqlquery);
        }
        try {
            $fql_obj = $this->facebook->api(array('method' => 'fql.query', 'query' => $fqlquery));
        } catch (FacebookApiException $e) {
            print $e->getMessage();
        }

        return $fql_obj;
    }
    

    public function logout(){        
        // Logs off session from website
        $this->facebook->destroySession();
        $this->facebook->setSession(NULL);
        // Make sure you destory website session as well.
        redirect('/', 'refresh');
    }
    
    private function iptocountry($ip) {
        $numbers = preg_split( "/\./", $ip);
        $url = "assets/ip_files/".$numbers[0].".php";
        include($url);
        $code=($numbers[0] * 16777216) + ($numbers[1] * 65536) + ($numbers[2] * 256) + ($numbers[3]);
        foreach($ranges as $key => $value){
            if($key<=$code){
                if($ranges[$key][0]>=$code){$two_letter_country_code=$ranges[$key][1];break;}
                }
        }
        if ($two_letter_country_code==""){$two_letter_country_code="unkown";}
        return $two_letter_country_code;
    }
}
