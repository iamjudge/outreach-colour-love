<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Coupons extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->library(array('template','email','session','common'));
        $this->load->model(array('admins','adminCoupons','adminPrizes','ccManager'));       
	}
	
	public function main(){
        if($_SESSION['access'] != true)redirect('/admin/login', 'refresh');
		$data['coupons'] = $this->adminCoupons->getCoupons();
		$this->template->write('title', 'Administrator');
		$this->template->set_template('admin');
		$this->template->write_view('header', 'templates/admin_header');
		$this->template->write_view('mainnav', 'templates/admin_menu');
		$this->template->write_view('content', 'admin/view_coupons', $data);
		$this->template->render();
	}
	
	public function prizes(){
		if($_SESSION['access'] != true)redirect('/admin/login', 'refresh');
		$data['prizes'] = $this->adminPrizes->getPrizes();
		$this->template->write('title', 'Administrator');
		$this->template->set_template('admin');
		$this->template->write_view('header', 'templates/admin_header');
		$this->template->write_view('mainnav', 'templates/admin_menu');
		$this->template->write_view('content', 'admin/view_prizes', $data);
		$this->template->render();
	}
	
	/** function that edits both coupon and prize **/
	public function editItem(){
		if($_SESSION['access'] != true)redirect('/admin/login', 'refresh');
		$itemID = $_GET['id']; $type = $_GET['type'];
		$data['countries'] = $this->ccManager->getCountries();
		$this->template->write('title', 'Edit Item');
		$this->template->set_template('admin');
		$this->template->write_view('header', 'templates/admin_header');
		$this->template->write_view('mainnav', 'templates/admin_menu');
		
		if($type == 1){
			$data['item'] = $this->adminPrizes->getPDetails($itemID);
			$data['prizes'] = $this->adminPrizes->getPrizes();
			$this->template->write_view('content', 'admin/edit_p_item', $data);
		} elseif($type == 2){
			$data['item'] = $this->adminCoupons->getCDetails($itemID);
			$data['coupons'] = $this->adminCoupons->getCoupons();
			$this->template->write_view('content', 'admin/edit_c_item', $data);
		}
		
		$this->template->render();
	}
	
	/** function that facilitates countries and city management **/
	public function ccManager(){
		if($_SESSION['access'] != true)redirect('/admin/login', 'refresh');
		$data['countries'] = $this->ccManager->getCountries();
		$data['cities'] = $this->ccManager->getCities();
		$this->template->write('title', 'Administrator');
		$this->template->set_template('admin');
		$this->template->write_view('header', 'templates/admin_header');
		$this->template->write_view('mainnav', 'templates/admin_menu');
		$this->template->write_view('content', 'admin/ccManager', $data);
		$this->template->render();
	}
	
	/** function to edit coupon and prize **/
	public function updateItems(){
		$this->adminPrizes->updateItem($this->input->post('id'),$this->input->post('type'));
		if($this->input->post('type') == 1){ $item = "Prize "; } elseif($this->input->post('type') == 2){ $item = "Coupon "; }
		echo $item."has been successfully updated.";
	}
	
	/** function to edit coupon and prize **/
	public function deleteItems(){
		$this->adminPrizes->deleteItem($this->input->get('id'),$this->input->get('type'));
		if($this->input->get('type') == 1){ $item = "Prize "; } elseif($this->input->get('type') == 2){ $item = "Coupon "; }
		echo "<script> alert('".$item."has been successfully deleted.'); </script>";
		if($this->input->get('type') == 1){ redirect('/coupons/prizes', 'refresh'); } elseif($this->input->get('type') == 2){ redirect('/coupons/main', 'refresh'); }
		redirect('/admin/login', 'refresh');
	}
	
	/** function that adds coupons **/
	public function addCoupon(){
		$arrayEntry = array_filter($this->adminCoupons->check_coupon($this->input->post('cCode')));
		if(!empty($arrayEntry)){
			echo "The coupon code has already been used. Please enter another code.";
		} else {
			$this->adminCoupons->insertCoupon();
			echo "Coupon has been successfully added!";
		}
	}
	
	/** function that adds prizes **/
	public function addPrize(){
		$this->adminPrizes->insertPrize();
		echo "Prize has been successfully added!";
	}
}

?>