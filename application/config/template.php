<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Active template
|--------------------------------------------------------------------------
|
| The $template['active_template'] setting lets you choose which template 
| group to make active.  By default there is only one group (the 
| "default" group).
|
*/
$template['active_template'] = 'main';

/*
|--------------------------------------------------------------------------
| Explaination of template group variables
|--------------------------------------------------------------------------
|
| ['template'] The filename of your master template file in the Views folder.
|   Typically this file will contain a full XHTML skeleton that outputs your
|   full template or region per region. Include the file extension if other
|   than ".php"
| ['regions'] Places within the template where your content may land. 
|   You may also include default markup, wrappers and attributes here 
|   (though not recommended). Region keys must be translatable into variables 
|   (no spaces or dashes, etc)
| ['parser'] The parser class/library to use for the parse_view() method
|   NOTE: See http://codeigniter.com/forums/viewthread/60050/P0/ for a good
|   Smarty Parser that works perfectly with Template
| ['parse_template'] FALSE (default) to treat master template as a View. TRUE
|   to user parser (see above) on the master template
|
| Region information can be extended by setting the following variables:
| ['content'] Must be an array! Use to set default region content
| ['name'] A string to identify the region beyond what it is defined by its key.
| ['wrapper'] An HTML element to wrap the region contents in. (We 
|   recommend doing this in your template file.)
| ['attributes'] Multidimensional array defining HTML attributes of the 
|   wrapper. (We recommend doing this in your template file.)
|
| Example:
| $template['default']['regions'] = array(
|    'header' => array(
|       'content' => array('<h1>Welcome</h1>','<p>Hello World</p>'),
|       'name' => 'Page Header',
|       'wrapper' => '<div>',
|       'attributes' => array('id' => 'header', 'class' => 'clearfix')
|    )
| );
|
*/

/*
|--------------------------------------------------------------------------
| Default Template Configuration (adjust this or create your own)
|--------------------------------------------------------------------------
*/

$template['main']['template'] = 'templates/default.php';
$template['main']['regions'] = array('title','content','mainnav','leftnav');
$template['main']['parser'] = 'parser';
$template['main']['parser_method'] = 'parse';
$template['main']['parse_template'] = TRUE;

$template['email']['template'] = 'templates/email.php';
$template['email']['regions'] = array('content');
$template['email']['parser'] = 'parser';
$template['email']['parser_method'] = 'parse';
$template['email']['parse_template'] = TRUE;

$template['admin']['template'] = 'templates/admin.php';
$template['admin']['regions'] = array('title','context','content','mainnav', 'header');
$template['admin']['parser'] = 'parser';
$template['admin']['parser_method'] = 'parse';
$template['admin']['parse_template'] = TRUE;

$template['admin_login']['template'] = 'templates/admin_login.php';
$template['admin_login']['regions'] = array('title','context','content','mainnav', 'header');
$template['admin_login']['parser'] = 'parser';
$template['admin_login']['parser_method'] = 'parse';
$template['admin_login']['parse_template'] = TRUE;



/* End of file template.php */
/* Location: ./system/application/config/template.php */