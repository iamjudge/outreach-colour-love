			<div id="sidebar" class="nav-collapse collapse">
				<div class="sidebar-toggler hidden-phone"></div>
				<div class="navbar-inverse">
					<form class="navbar-search visible-phone">
						<input type="text" class="search-query" placeholder="Search" />
					</form>
				</div>
				<ul class="sidebar-menu">
					<li><a href="<?php echo base_url('admin'); ?>" class=""><span class="icon-box"><i class="icon-dashboard"></i></span> Dashboard</a></li>
					<li class="has-sub">
						<a href="javascript:;" class=""><span class="icon-box"><i class="icon-facebook-sign"></i></span> Promotions <span class="arrow"></span></a>
						<ul class="sub">
							<li><a class="" href="<?php echo base_url('coupons/main'); ?>"> Coupons</a></li>
							<li><a class="" href="<?php echo base_url('coupons/prizes'); ?>"> Coupon Prizes</a></li>
							<li><a class="" href="<?php echo base_url('admin/users'); ?>"> Registered Users</a></li>
							<li><a class="" href="<?php echo base_url('coupons/ccManager'); ?>"> City &amp; Country Manager</a></li>
						</ul>
					</li>
					<li><a href="<?php echo base_url('admin'); ?>" class=""><span class="icon-box"><i class="icon-dashboard"></i></span> Events</a></li>
				</ul>
			</div>