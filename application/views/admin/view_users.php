<div class="row-fluid">
	<div class="span12">
		<h3 class="page-title">Promotion Users</h3>
		<ul class="breadcrumb">
			<li>
				<a href="#"><i class="icon-home"></i></a>
				<span class="divider">&nbsp;</span>
			</li>
			<li>
				<a href="#">Promotions</a>
				<span class="divider">&nbsp;</span>
			</li>
			<li>
				<a href="#">View Users</a>
				<span class="divider-last">&nbsp;</span>
			</li>
		</ul>
	</div>
</div>

<div class="row-fluid">
	<div class="span6">
		<div class="widget">
			<div class="widget-title">
				<h4><i class="icon-reorder"></i> Quick Add User</h4>
				<span class="tools">
					<a href="javascript:;" class="icon-chevron-down"></a>
					<a href="javascript:;" class="icon-remove"></a>
				</span>
			</div>
			<div class="widget-body">
				
				<form action="#" class="form-horizontal" />
					<div class="control-group">
						<label class="control-label">User Email</label>
						<div class="controls">
							<input type="text" class="input" />
						</div>
					</div>
					
					<div class="control-group">
						<label class="control-label">&nbsp;</label>
						<div class="controls">
							<button class="btn btn-success"><i class="icon-ok icon-white"></i> Add User</button>
						</div>
					</div>
				</form>
				
			</div>
		</div>
	</div>
	
	<div class="span6">
		<div class="widget">
			<div class="widget-title">
				<h4><i class="icon-reorder"></i> Quick Add User</h4>
				<span class="tools">
					<a href="javascript:;" class="icon-chevron-down"></a>
					<a href="javascript:;" class="icon-remove"></a>
				</span>
			</div>
			<div class="widget-body">
				
				<form action="#" class="form-horizontal" />
					<div class="control-group">
						<label class="control-label">User Email</label>
						<div class="controls">
							<input type="text" class="input" />
						</div>
					</div>
					
					<div class="control-group">
						<label class="control-label">&nbsp;</label>
						<div class="controls">
							<button class="btn btn-success"><i class="icon-ok icon-white"></i> Add User</button>
						</div>
					</div>
				</form>
				
			</div>
		</div>
	</div>
</div>

<div class="row-fluid">
	<div class="span12">
		<div class="widget">
			<div class="widget-title">
				<h4><i class="icon-reorder"></i> View All Users</h4>
				<span class="tools">
					<a href="javascript:;" class="icon-chevron-down"></a>
					<a href="javascript:;" class="icon-remove"></a>
				</span>
			</div>
			<div class="widget-body">
				
				<div style="margin-bottom:10px;">
					<div class="btn-group">
						<button class="btn dropdown-toggle" data-toggle="dropdown">Action <span class="caret"></span></button>
						<ul class="dropdown-menu">
							<li><a href="#">Delete selected</a></li>
							<li class="divider"></li>
							<li><a href="#">Set status to disabled</a></li>
							<li><a href="#">Set status to enabled</a></li>
						</ul>
					</div>
				</div>
				
				<table class="table table-striped table-bordered" id="sample_1">
					<thead>
						<tr>
							<th style="width:8px;"><input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" /></th>
							<th class="hidden-phone">NAME</th>
							<th class="hidden-phone">EMAIL ADDRESS</th>
							<th class="hidden-phone">FACEBOOK URL</th>
							<th class="hidden-phone">DATE JOINED</th>
							<th class="hidden-phone">STATUS</th>
						</tr>
					</thead>
					<tbody>
						<tr class="odd gradeX">
							<td><input type="checkbox" class="checkboxes" value="1" /></td>
							<td class="hidden-phone">Judge Zulueta</td>
							<td class="hidden-phone">jcz@iamjudge.net</td>
							<td class="hidden-phone">iamjudge</td>
							<td class="hidden-phone">January 14, 2014</td>
							<td class="hidden-phone"><span class="label label-success">Enabled</span></td>
						</tr>
						<tr class="odd gradeX">
							<td><input type="checkbox" class="checkboxes" value="1" /></td>
							<td class="hidden-phone">Arniel Pepito</td>
							<td class="hidden-phone">jcz@iamjudge.net</td>
							<td class="hidden-phone">iamjudge</td>
							<td class="hidden-phone">January 14, 2014</td>
							<td class="hidden-phone"><span class="label label-success">Enabled</span></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>


<div id="createCoupon" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
		<h3 id="myModalLabel4">Add a Prize</h3>
	</div>
	<div class="modal-body">
		<div class="widget-body">
			<form action="#" class="form-horizontal" />
				<div class="control-group">
					<label class="control-label">Prize Name</label>
					<div class="controls">
						<input type="text" class="input-xlarge" />
						
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">Prize Description</label>
					<div class="controls">
						<textarea class="input-xlarge" rows="3"></textarea>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">Item Code</label>
					<div class="controls">
						<input type="text" class="input-xlarge" />
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">Level</label>
					<div class="controls">
						<input type="text" class="input-xlarge" />
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">Shipping Details</label>
					<div class="controls">
						<textarea class="input-xlarge" rows="3"></textarea>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div class="modal-footer">
		<button class="btn btn-error" data-dismiss="modal" aria-hidden="true">Cancel</button>
		<button data-dismiss="modal" class="btn btn-success">Add Prize</button>
	</div>
</div>