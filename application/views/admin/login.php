<div class="login-header">
	<div id="logo" class="center">
		<img src="<?php echo base_url('/img/logo.png')?>" alt="logo" class="center" />
	</div>
</div>
<div id="login">
    <div class="widget-body">
        <?php 
        if($_SESSION['error']){           
        ?>
        <div class="alert alert-error">
            <button class="close" data-dismiss="alert">�</button>
            <?php print $_SESSION['error'][1]; ?>
        </div>
        <?php 
        $_SESSION['error'] = array();
        }
        ?>
    </div>
	<form method="post" class="form-vertical no-padding no-margin" action="<?php echo base_url('/admin/login')?>">
		<div class="lock"><i class="icon-lock"></i></div>
		<div class="control-wrap">
			<h4>User Login</h4>
			<div class="control-group">
				<div class="controls">
					<div class="input-prepend">
						<span class="add-on"><i class="icon-user"></i></span>
						<input id="input-username" name="username" type="text" placeholder="Username" />
					</div>
				</div>
			</div>			
			<div class="control-group">
				<div class="controls">
					<div class="input-prepend">
						<span class="add-on"><i class="icon-key"></i></span>
						<input id="input-password" name="password" type="password" placeholder="Password" />
					</div>
					<div class="mtop10">
						<div class="block-hint pull-left small"><input type="checkbox" id="" />Remember Me </div>
						<div class="block-hint pull-right"><a href="javascript:;" class="" id="forget-password">Forgot Password?</a></div>
					</div>
					<div class="clearfix space5"></div>
				</div>
			</div>
		</div>
		<input type="submit" id="login-btn" class="btn btn-block login-btn" value="Login" />
	</form>
	
	<form id="forgotform" class="form-vertical no-padding no-margin hide" action="index.html" />
		<p class="center">Enter your e-mail address below to reset your password.</p>
		<div class="control-group">
			<div class="controls">
				<div class="input-prepend">
					<span class="add-on"><i class="icon-envelope"></i></span>
					<input id="input-email" type="text" placeholder="Email" />
				</div>
			</div>
			<div class="space20"></div>
		</div>
		<input type="button" id="forget-btn" class="btn btn-block login-btn" value="Submit" />
	</form>
	
</div>


<div id="login-copyright">2014 &amp; beyond &copy; Judge is Awesome. </div>