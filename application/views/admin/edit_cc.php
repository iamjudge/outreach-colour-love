<?php
	$all_coupons = "";
	foreach($coupons as $items){
		switch($items['status']){
			case 0: $status = "<span class='label label-important'>Disabled</span>"; break;
			case 1: $status = "<span class='label label-success'>Enabled</span>"; break;
			case 2: $status = "<span class='label label-success'>Pending</span>"; break;
		}
		$all_coupons .= '
			<tr class="odd gradeX">
				<td class="hidden-phone"><a href="editItem?id='.$items['id'].'&type=2">'.$items['code'].'</a></td>
				<td class="hidden-phone">'.$items['level'].'</td>
				<td class="hidden-phone">'.$items['prize'].'</td>
				<td class="hidden-phone">'.$status.'</td>
			</tr>
		';
	}
?>

<div class="row-fluid">
	<div class="span12">
		<h3 class="page-title"> Edit Coupon</h3>
		<ul class="breadcrumb">
			<li>
				<a href="#"><i class="icon-home"></i></a>
				<span class="divider">&nbsp;</span>
			</li>
			<li>
				<a href="#">Promotions</a>
				<span class="divider">&nbsp;</span>
			</li>
			<li>
				<a href="#">Edit Coupon</a>
				<span class="divider-last">&nbsp;</span>
			</li>
		</ul>
	</div>
</div>

<div class="row-fluid">
	<div class="span7">
		<div class="widget">
			<div class="widget-title">
				<h4><i class="icon-reorder"></i>Edit Coupon</h4>
				<span class="tools">
					<a href="javascript:;" class="icon-chevron-down"></a>
					<a href="javascript:;" class="icon-remove"></a>
				</span>
			</div>
			<div class="widget-body">
				<form action="#" class="form-horizontal" id="updateItemForm" method="post">
					<div class="widget-body">
						<div class="control-group">
							<label class="control-label">Coupon Code</label>
							<div class="controls">
								<input type="text" class="input-large" id="coupon_code" name="coupon_code" value="<?php echo $item[0]['code']; ?>" readonly/>
								<input type="hidden" class="input-xlarge" name="id" id="id" value="<?php echo $item[0]['id']; ?>"/>
								<input type="hidden" class="input-xlarge" name="type" id="type" value="2"/>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Coupon Description</label>
							<div class="controls">
								<textarea name="coupon_description" id="coupon_description" class="input-large"><?php echo $item[0]['description']; ?></textarea>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Level</label>
							<div class="controls">
								<select class="input-large" name="level" id="level">
									<option value="Small Prize" <?php if($item[0]['level'] == "Small Prize"){ echo "selected"; } ?>>Small Prize</option>
									<option value="Medium Prize" <?php if($item[0]['level'] == "Medium Prize"){ echo "selected"; } ?>>Medium Prize</option>
									<option value="Big Prize" <?php if($item[0]['level'] == "Big Prize"){ echo "selected"; } ?>>Big Prize</option>
								</select>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Prize</label>
							<div class="controls">
								<input type="text" class="input-large" name="prize" id="prize" value="<?php echo $item[0]['prize']; ?>"/>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Frequency Rate</label>
							<div class="controls">
								<input type="text" class="input-large" name="frequency_rate" id="frequency_rate" value="<?php echo $item[0]['frequencyRate']; ?>"/>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Frequency Number</label>
							<div class="controls">
								<input type="text" class="input-large" name="frequency_num" id="frequency_num" value="<?php echo $item[0]['frequencyTimeNum']; ?>"/>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Frequency Time Rate</label>
							<div class="controls">
								<input type="text" class="input-large" name="frequency_time_rate" id="frequency_time_rate" value="<?php echo $item[0]['frequencyTimeRate']; ?>"/>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Expiration</label>
							<div class="controls">
								<input type="text" class="input-large" name="expiration" id="expiration" value="<?php echo $item[0]['expiration']; ?>"/>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Status</label>
							<div class="controls">
								<select name="status" id="status">
									<option value="0" <?php if($item[0]['status'] == 0){ echo "selected"; } ?>>Disabled</option>
									<option value="1" <?php if($item[0]['status'] == 1){ echo "selected"; } ?>>Enabled</option>
									<option value="2" <?php if($item[0]['status'] == 2){ echo "selected"; } ?>>Pending</option>
								</select>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Restrictions</label>
							<div class="controls">
								<input type="text" class="input-large" name="restrictions" id="restrictions" value="<?php echo $item[0]['restrictions']; ?>" readonly/>
							</div>
						</div>	
						<div class="control-group">
							<label class="control-label">&nbsp;</label>
							<div class="controls">
								<div id="updateItem" class="btn btn-success">Update Coupon</div>
								<a href="deleteItems?id=<?php echo $item[0]['id']; ?>&type=2" class="btn btn-danger">Delete Coupon</a>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	
	<div class="span5">
		<div class="widget">
			<div class="widget-title">
				<h4><i class="icon-reorder"></i>View All Coupons</h4>
				<span class="tools">
					<a href="javascript:;" class="icon-chevron-down"></a>
					<a href="javascript:;" class="icon-remove"></a>
				</span>
			</div>
			<div class="widget-body">
				<table class="table table-striped table-bordered" id="sample_1">
					<thead>
						<tr>
							<th class="hidden-phone">CODE</th>
							<th class="hidden-phone">LEVEL</th>
							<th class="hidden-phone">PRIZE</th>
							<th class="hidden-phone">STATUS</th>
						</tr>
					</thead>
					<tbody>
						<?php echo $all_coupons; unset($all_coupons); ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>