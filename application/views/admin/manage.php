<div class="row-fluid">
	<div class="span12">
		<h3 class="page-title">Promotion Users</h3>
		<ul class="breadcrumb">
			<li>
				<a href="#"><i class="icon-home"></i></a>
				<span class="divider">&nbsp;</span>
			</li>
			<li>
				<a href="<?php echo base_url('/admin/')?>">Dashboard</a>
				<span class="divider">&nbsp;</span>
			</li>
			<li>
				<a href="#">Manage Admins</a>
				<span class="divider-last">&nbsp;</span>
			</li>
		</ul>
	</div>
</div>
<?php 
if($_SESSION['error']){           
?>
<div class="widget-body">
    <div class="alert alert-error">
        <button class="close" data-dismiss="alert">�</button>
        <?php print $_SESSION['error'][1]; ?>
    </div>
</div>
<?php 
$_SESSION['error'] = array();
}
?>

<div class="row-fluid">
	<div class="span6">
		<div class="widget">
			<div class="widget-title">
				<h4><i class="icon-reorder"></i> Quick Add Admin</h4>
				<span class="tools">
					<a href="javascript:;" class="icon-chevron-down"></a>
					<a href="javascript:;" class="icon-remove"></a>
				</span>
			</div>
			<div class="widget-body">
				
				<form action="#" method="POST" class="form-horizontal" />
					<div class="control-group">
						<label class="control-label">Name</label>
						<div class="controls">
							<input type="text" name="name" class="input" />
                            <input type="hidden" name="type" value="add" />
						</div>
					</div>
                    <div class="control-group">
						<label class="control-label">Username</label>
						<div class="controls">
							<input type="text" name="username" class="input" />
						</div>
					</div>
                    <div class="control-group">
						<label class="control-label">Password</label>
						<div class="controls">
							<input type="password" name="password" class="input" />
						</div>
					</div>
                    <div class="control-group">
                        <label class="control-label">Email Address</label>
                        <div class="controls">
                            <div class="input-prepend">
                                <span class="add-on">@</span>
                                <input class=" " type="text" name="email" placeholder="Email Address">
                            </div>
                        </div>
                    </div>					
					<div class="control-group">
						<label class="control-label">&nbsp;</label>
						<div class="controls">
							<button class="btn btn-success"><i class="icon-ok icon-white"></i> Add </button>
						</div>
					</div>
				</form>
				
			</div>
		</div>
	</div>
</div>

<div class="row-fluid">
	<div class="span12">
		<div class="widget">
			<div class="widget-title">
				<h4><i class="icon-reorder"></i> View All Admins</h4>
				<span class="tools">
					<a href="javascript:;" class="icon-chevron-down"></a>
					<a href="javascript:;" class="icon-remove"></a>
				</span>
			</div>
			<div class="widget-body">				
				<table class="table table-striped table-bordered" id="sample_1">
					<thead>
						<tr>
							<th class="hidden-phone">NAME</th>
                            <th>USERNAME</th>
							<th class="hidden-phone">EMAIL</th>
							<th class="hidden-phone">LAST LOGIN</th>
							<th class="hidden-phone">DATE JOINED</th>
							<th>STATUS</th>
                            <th>MANAGE</th>
						</tr>
					</thead>
					<tbody>
                        <?php
                        foreach($admins as $a){
                        ?>
						<tr class="odd gradeX">
							<td class="hidden-phone"><?php print $a['name'];?></td>
                            <td ><?php print $a['username'];?></td>
							<td class="hidden-phone"><?php print $a['email'];?></td>
							<td class="hidden-phone"><?php print $a['lastLogin'];?></td>
                            <td class="hidden-phone"><?php print $a['dateCreated'];?></td>
							<td>
                                <?php if((int)$a['status']==1):?>
                                <a class="label label-success" href="#" title="Block" onclick="location.href='<?php echo base_url("/admin/admins?adminid=".$a['id']."&status=0&type=update")?>'">Active</a>
                                <?php else:?>
                                <a class="label label-failed" href="#" title="Activate" onclick="location.href='<?php echo base_url("/admin/admins?adminid=".$a['id']."&status=1&type=update")?>'">Block</a>
                                <?php endif;?>
                            </td>
                            <td>
                                <button class="btn btn-mini btn-primary" type="button">Edit</button>
                                <button class="btn btn-mini" type="button">Delete</button>
                            </td>
						</tr>
                        <?php 
                        }
                        ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>


<div id="createCoupon" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
		<h3 id="myModalLabel4">Add a Prize</h3>
	</div>
	<div class="modal-body">
		<div class="widget-body">
			<form action="#" class="form-horizontal" />
				<div class="control-group">
					<label class="control-label">Prize Name</label>
					<div class="controls">
						<input type="text" class="input-xlarge" />
						
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">Prize Description</label>
					<div class="controls">
						<textarea class="input-xlarge" rows="3"></textarea>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">Item Code</label>
					<div class="controls">
						<input type="text" class="input-xlarge" />
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">Level</label>
					<div class="controls">
						<input type="text" class="input-xlarge" />
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">Shipping Details</label>
					<div class="controls">
						<textarea class="input-xlarge" rows="3"></textarea>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div class="modal-footer">
		<button class="btn btn-error" data-dismiss="modal" aria-hidden="true">Cancel</button>
		<button data-dismiss="modal" class="btn btn-success">Add Prize</button>
	</div>
</div>