<?php
	$all_prizes = "";
	foreach($prizes as $items){
		switch($items['status']){
			case 0:
				$status = "<span class='label label-important'>Disabled</span>";
				break;
			case 1:
				$status = "<span class='label label-success'>Enabled</span>";
				break;
			case 2:
				$status = "<span class='label label-success'>Pending</span>";
				break;
		}
		$all_prizes .= '
			<tr class="odd gradeX">
				<td><input type="checkbox" class="checkboxes" value="1" /></td>
				<td><a href="editItem?id='.$items['id'].'&type=1">'.$items['prizeName'].'</a></td>
				<td class="hidden-phone">'.$items['code'].'</td>
				<td class="hidden-phone">'.$items['prizeLevel'].'</td>
				<td class="hidden-phone">'.$status.'</td>
			</tr>
		';
	}
?>

<div class="row-fluid">
	<div class="span12">
		<h3 class="page-title"> Promotion Prizes</h3>
		<ul class="breadcrumb">
			<li>
				<a href="#"><i class="icon-home"></i></a>
				<span class="divider">&nbsp;</span>
			</li>
			<li>
				<a href="#">Promotions</a>
				<span class="divider">&nbsp;</span>
			</li>
			<li>
				<a href="#">Prizes</a>
				<span class="divider-last">&nbsp;</span>
			</li>
		</ul>
	</div>
</div>

<div class="row-fluid">
	<div class="span12">
		<div class="widget">
			<div class="widget-title">
				<h4><i class="icon-reorder"></i>View All Prizes</h4>
				<span class="tools">
					<a href="javascript:;" class="icon-chevron-down"></a>
					<a href="javascript:;" class="icon-remove"></a>
				</span>
			</div>
			<div class="widget-body">
				
				<div style="margin-bottom:10px;">
					<a href="#createCoupon" role="button" class="btn btn-success" data-toggle="modal"><b>Add a Prize</b></a>
					<div class="btn-group">
						<button class="btn dropdown-toggle" data-toggle="dropdown">Action <span class="caret"></span></button>
						<ul class="dropdown-menu">
							<li><a href="#">Delete selected</a></li>
							<li class="divider"></li>
							<li><a href="#">Set status to disabled</a></li>
							<li><a href="#">Set status to enabled</a></li>
						</ul>
					</div>
				</div>
				
				<table class="table table-striped table-bordered" id="sample_2">
					<thead>
						<tr>
							<th style="width:8px;"><input type="checkbox" class="group-checkable" data-set="#sample_2 .checkboxes" /></th>
							<th class="hidden-phone">PRIZE NAME</th>
							<th class="hidden-phone">CODE</th>
							<th class="hidden-phone">LEVEL</th>
							<th class="hidden-phone">STATUS</th>
						</tr>
					</thead>
					<tbody>
						<?php echo $all_prizes; ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>


<div id="createCoupon" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
		<h3 id="myModalLabel4">Add a Prize</h3>
	</div>
	<form action="#" class="form-horizontal" />
		<div class="modal-body">
			<div class="widget-body">
				<div class="control-group">
					<label class="control-label">Prize Name</label>
					<div class="controls">
						<input type="text" class="input-xlarge" name="prize_name" id="prize_name"/>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">Prize Description</label>
					<div class="controls">
						<textarea class="input-xlarge" rows="3" name="description" id="description"></textarea>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">Item Code</label>
					<div class="controls">
						<input type="text" class="input-xlarge" name="code" id="code"/>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">Level</label>
					<div class="controls">
						<input type="text" class="input-xlarge" name="prize_level" id="prize_level"/>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">Shipping Details</label>
					<div class="controls">
						<textarea class="input-xlarge" rows="3" name="shipping_details" id="shipping_details"></textarea>
					</div>
				</div>			
				<div class="control-group">
					<label class="control-label">Status</label>
					<div class="controls">
						<select name="status" id="status">
							<option value="0">Disabled</option>
							<option value="1">Enabled</option>
							<option value="2">Pending</option>
						</select>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">Stock</label>
					<div class="controls">
						<input type="text" class="input-large" name="stock" id="stock"/>
					</div>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button class="btn btn-error" data-dismiss="modal" aria-hidden="true">Cancel</button>
			<button data-dismiss="modal" id="addPrize" class="btn btn-success">Add Prize</button>
		</div>
	</form>
</div>



<div id="judge"></div>