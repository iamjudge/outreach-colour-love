<?php
	$all_cities = ""; $all_countries = "";
	foreach($cities as $items){
		$countryCity = $this->ccManager->getSpecificCountry($items['country']);
		$all_cities .= '
			<tr class="odd gradeX">
				<td class="hidden-phone"><a href="#">'.$items['id'].'</a></td>
				<td class="hidden-phone"><a href="#">'.$items['city'].'</a></td>
				<td class="hidden-phone">'.$countryCity[0]['country'].'</td>
				<td class="hidden-phone">EDIT DELETE</td>
			</tr>
		';
	}
	foreach($countries as $items){
		$all_countries .= '
			<tr class="odd gradeX">
				<td class="hidden-phone"><a href="#">'.$items['id'].'</a></td>
				<td class="hidden-phone"><a href="#">'.$items['country'].'</a></td>
				<td class="hidden-phone">EDIT DELETE</td>
			</tr>
		';
	}
?>

<div class="row-fluid">
	<div class="span12">
		<h3 class="page-title"> City &amp; Country Manager</h3>
		<ul class="breadcrumb">
			<li>
				<a href="#"><i class="icon-home"></i></a>
				<span class="divider">&nbsp;</span>
			</li>
			<li>
				<a href="#">Promotions</a>
				<span class="divider">&nbsp;</span>
			</li>
			<li>
				<a href="#">City &amp; Country Manager</a>
				<span class="divider-last">&nbsp;</span>
			</li>
		</ul>
	</div>
</div>

<div class="row-fluid">
	<div class="span6">
		<div class="widget">
			<div class="widget-title">
				<h4><i class="icon-reorder"></i> Countries</h4>
				<span class="tools">
					<a href="javascript:;" class="icon-chevron-down"></a>
					<a href="javascript:;" class="icon-remove"></a>
				</span>
			</div>
			<div class="widget-body">
				<table class="table table-striped table-bordered" id="sample_1">
					<thead>
						<tr>
							<th class="hidden-phone">ID</th>
							<th class="hidden-phone">COUNTRY</th>
							<th class="hidden-phone">ACTION</th>
						</tr>
					</thead>
					<tbody>
						<?php echo $all_countries; ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	
	<div class="span6">
		<div class="widget">
			<div class="widget-title">
				<h4><i class="icon-reorder"></i> Cities</h4>
				<span class="tools">
					<a href="javascript:;" class="icon-chevron-down"></a>
					<a href="javascript:;" class="icon-remove"></a>
				</span>
			</div>
			<div class="widget-body">
				<table class="table table-striped table-bordered" id="sample_1">
					<thead>
						<tr>
							<th class="hidden-phone">ID</th>
							<th class="hidden-phone">CITY</th>
							<th class="hidden-phone">COUNTRY</th>
							<th class="hidden-phone">ACTION</th>
						</tr>
					</thead>
					<tbody>
						<?php echo $all_cities; ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>