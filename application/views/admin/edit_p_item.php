<?php
	$all_prizes = "";
	foreach($prizes as $items){
		switch($items['status']){
			case 0:
				$status = "<span class='label label-important'>Disabled</span>";
				break;
			case 1:
				$status = "<span class='label label-success'>Enabled</span>";
				break;
			case 2:
				$status = "<span class='label label-success'>Pending</span>";
				break;
		}
		$all_prizes .= '
			<tr class="odd gradeX">
				<td><a href="editItem?id='.$items['id'].'&type=1">'.$items['prizeName'].'</a></td>
				<td class="hidden-phone">'.$items['code'].'</td>
				<td class="hidden-phone">'.$items['prizeLevel'].'</td>
				<td class="hidden-phone">'.$status.'</td>
			</tr>
		';
	}
?>

<div class="row-fluid">
	<div class="span12">
		<h3 class="page-title"> Edit Prize</h3>
		<ul class="breadcrumb">
			<li>
				<a href="#"><i class="icon-home"></i></a>
				<span class="divider">&nbsp;</span>
			</li>
			<li>
				<a href="#">Promotions</a>
				<span class="divider">&nbsp;</span>
			</li>
			<li>
				<a href="#">Edit Prize</a>
				<span class="divider-last">&nbsp;</span>
			</li>
		</ul>
	</div>
</div>

<div class="row-fluid">
	<div class="span7">
		<div class="widget">
			<div class="widget-title">
				<h4><i class="icon-reorder"></i>Edit Prize</h4>
				<span class="tools">
					<a href="javascript:;" class="icon-chevron-down"></a>
					<a href="javascript:;" class="icon-remove"></a>
				</span>
			</div>
			<div class="widget-body">
				<form action="#" class="form-horizontal" id="updateItemForm" method="post">
					<div class="widget-body">
						<div class="control-group">
							<label class="control-label">Prize Name</label>
							<div class="controls">
								<input type="hidden" class="input-xlarge" name="id" id="id" value="<?php echo $item[0]['id']; ?>"/>
								<input type="hidden" class="input-xlarge" name="type" id="type" value="1"/>
								<input type="text" class="input-xlarge" name="prizeName" id="prizeName" value="<?php echo $item[0]['prizeName']; ?>"/>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Prize Description</label>
							<div class="controls">
								<textarea class="input-xlarge" rows="3" name="description" id="description"><?php echo $item[0]['description']; ?></textarea>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Item Code</label>
							<div class="controls">
								<input type="text" class="input-xlarge" name="code" id="code" value="<?php echo $item[0]['code']; ?>" disabled/>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Level</label>
							<div class="controls">
								<input type="text" class="input-xlarge" name="prize_level" id="prize_level" value="<?php echo $item[0]['prizeLevel']; ?>"/>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Shipping Details</label>
							<div class="controls">
								<textarea class="input-xlarge" rows="3" name="shipping_details" id="shipping_details"><?php echo $item[0]['shippingDetails']; ?></textarea>
							</div>
						</div>			
						<div class="control-group">
							<label class="control-label">Status</label>
							<div class="controls">
								<select name="status" id="status">
									<option value="0" <?php if($item[0]['status'] == 0){ echo "selected"; } ?>>Disabled</option>
									<option value="1" <?php if($item[0]['status'] == 1){ echo "selected"; } ?>>Enabled</option>
									<option value="2" <?php if($item[0]['status'] == 2){ echo "selected"; } ?>>Pending</option>
								</select>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Stock</label>
							<div class="controls">
								<input type="text" class="input-large" name="stock" id="stock" value="<?php echo $item[0]['stock']; ?>"/>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">&nbsp;</label>
							<div class="controls">
								<div id="updateItem" class="btn btn-success">Update Prize</div>
								<a href="deleteItems?id=<?php echo $item[0]['id']; ?>&type=1" class="btn btn-danger">Delete Prize</a>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	
	<div class="span5">
		<div class="widget">
			<div class="widget-title">
				<h4><i class="icon-reorder"></i>View All Prizes</h4>
				<span class="tools">
					<a href="javascript:;" class="icon-chevron-down"></a>
					<a href="javascript:;" class="icon-remove"></a>
				</span>
			</div>
			<div class="widget-body">
				<table class="table table-striped table-bordered" id="sample_2">
					<thead>
						<tr>
							<th class="hidden-phone">PRIZE NAME</th>
							<th class="hidden-phone">CODE</th>
							<th class="hidden-phone">LEVEL</th>
							<th class="hidden-phone">STATUS</th>
						</tr>
					</thead>
					<tbody>
						<?php echo $all_prizes; ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>