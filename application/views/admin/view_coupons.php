<?php
	$all_coupons = "";
	foreach($coupons as $items){
		switch($items['status']){
			case 0:
				$status = "<span class='label label-important'>Disabled</span>";
				break;
			case 1:
				$status = "<span class='label label-success'>Enabled</span>";
				break;
			case 2:
				$status = "<span class='label label-success'>Pending</span>";
				break;
		}
		$all_coupons .= '
			<tr class="odd gradeX">
				<td><input type="checkbox" class="checkboxes" value="1" /></td>
				<td>'.$items['id'].'</td>
				<td class="hidden-phone"><a href="editItem?id='.$items['id'].'&type=2">'.$items['code'].'</a></td>
				<td class="hidden-phone">'.$items['description'].'</td>
				<td class="hidden-phone">'.$items['level'].'</td>
				<td class="hidden-phone">'.$items['prize'].'</td>
				<td class="hidden-phone">'.$items['frequencyRate'].' times every '.$items['frequencyTimeNum'].' '.$items['frequencyTimeRate'].'</td>
				<td class="hidden-phone">'.$items['expiration'].'</td>
				<td class="hidden-phone">'.$status.'</td>
			</tr>
		';
	}
?>

<div class="row-fluid">
	<div class="span12">
		<h3 class="page-title"> Promotion Coupons</h3>
		<ul class="breadcrumb">
			<li>
				<a href="#"><i class="icon-home"></i></a>
				<span class="divider">&nbsp;</span>
			</li>
			<li>
				<a href="#">Promotions</a>
				<span class="divider">&nbsp;</span>
			</li>
			<li>
				<a href="#">Coupons</a>
				<span class="divider-last">&nbsp;</span>
			</li>
		</ul>
	</div>
</div>

<div class="row-fluid">
	<div class="span12">
		<div class="widget">
			<div class="widget-title">
				<h4><i class="icon-reorder"></i>View All Coupons</h4>
				<span class="tools">
					<a href="javascript:;" class="icon-chevron-down"></a>
					<a href="javascript:;" class="icon-remove"></a>
				</span>
			</div>
			<div class="widget-body">
				
				<div style="margin-bottom:10px;">
					<a href="#createCoupon" role="button" class="btn btn-success" data-toggle="modal"><b>Create Coupon</b></a>
					
					<div class="btn-group">
						<button class="btn dropdown-toggle" data-toggle="dropdown">Action <span class="caret"></span></button>
						<ul class="dropdown-menu">
							<li><a href="#">Delete selected</a></li>
							<li class="divider"></li>
							<li><a href="#">Set status to disabled</a></li>
							<li><a href="#">Set status to enabled</a></li>
						</ul>
					</div>
				</div>
				
				<table class="table table-striped table-bordered" id="sample_1">
					<thead>
						<tr>
							<th style="width:8px;"><input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" /></th>
							<th>ID</th>
							<th class="hidden-phone">CODE</th>
							<th class="hidden-phone">DESCRIPTION</th>
							<th class="hidden-phone">LEVEL</th>
							<th class="hidden-phone">PRIZE</th>
							<th class="hidden-phone">AVAILABILITY</th> <!-- frequencyRate -- frequencyTimeNum -- frequencyTimeRate-->
							<th class="hidden-phone">EXPIRATION</th>
							<th class="hidden-phone">STATUS</th>
						</tr>
					</thead>
					<tbody>
						<?php echo $all_coupons; unset($all_coupons); ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>


<div id="createCoupon" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
		<h3 id="myModalLabel4">Add a Coupon</h3>
	</div>
	<form action="#" class="form-horizontal" />
		<div class="modal-body">
			<div class="widget-body">
				<div class="control-group">
					<label class="control-label">Coupon Code</label>
					<div class="controls">
						<input type="text" class="input-large" id="coupon_code" name="coupon_code"/>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">Coupon Description</label>
					<div class="controls">
						<textarea name="coupon_description" id="coupon_description" class="input-large"></textarea>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">Level</label>
					<div class="controls">
						<select class="input-large" name="level" id="level">
							<option value="Small Prize">Small Prize</option>
							<option value="Medium Prize">Medium Prize</option>
							<option value="Big Prize">Big Prize</option>
						</select>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">Prize</label>
					<div class="controls">
						<input type="text" class="input-large" name="prize" id="prize"/>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">Frequency Rate</label>
					<div class="controls">
						<input type="text" class="input-large" name="frequency_rate" id="frequency_rate"/>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">Frequency Number</label>
					<div class="controls">
						<input type="text" class="input-large" name="frequency_num" id="frequency_num"/>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">Frequency Time Rate</label>
					<div class="controls">
						<input type="text" class="input-large" name="frequency_time_rate" id="frequency_time_rate"/>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">Expiration</label>
					<div class="controls">
						<input type="text" class="input-large" name="expiration" id="expiration"/>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">Status</label>
					<div class="controls">
						<select name="status" id="status">
							<option value="0">Disabled</option>
							<option value="1">Enabled</option>
							<option value="2">Pending</option>
						</select>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">Restrictions</label>
					<div class="controls">
						<input type="text" class="input-large" name="restrictions" id="restrictions"/>
					</div>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button class="btn btn-error" data-dismiss="modal" aria-hidden="true">Cancel</button>
			<button id="addCoupon" class="btn btn-success" data-dismiss="modal">Add Coupon</button>
		</div>
	</form>
</div>