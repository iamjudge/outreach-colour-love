<!DOCTYPE html>
<html xmlns:fb="http://www.facebook.com/2008/fbml">
  <head>
        <title>Slot Machine</title>
        <link rel="stylesheet" href="<?php echo base_url('/assets/game/css/reset.css')?>">
        <link rel="stylesheet" href="<?php echo base_url('/assets/game/css/slot.css')?>">
        <script>
            var baseURL = "<?php echo base_url('/welcome/setstatus')?>";
        </script>
        <script src="<?php echo base_url('/assets/game/js/jquery-1.8.1.min.js')?>"></script>
        <script src="<?php echo base_url('/assets/game/js/jquery.slots.js')?>"></script>        
  </head>
  <body>
    <?php if ($user) { ?>
      <div style="float:left; font-size:12px; padding:20px;"> 
        Hello <strong style="font-weight:bold;"> <?php print $user_profile['name'];?></strong>             
        <br/><br/>
        Given three (3) chances you can test your luck on getting high score on the slot machine. The price will be posted on the user timeline.
        <br/><br/>
        <strong style="font-size: 16px; font-weight:bold;">Comment this website.</strong>
        <br/>
        <fb:comments href="https://www.facebook.com/colourlove5k" width="350" numposts="5" colorscheme="light"></fb:comments>
        <br/>
        <!-- List of my friends that are using colourlove promotion app.-->   
        <br/>
        <?php
        /*
        foreach($list as $u):
            print '<div style="width:100px; float:left;"><img src="https://graph.facebook.com/'.$u['username'].'/picture"><br/>';
            print  $u['name'];
            print  "</div>";
        endforeach;
        */
        ?>
        <div style="clear:both;"></div>
        <br/>
        <br/>
        <strong style="font-size: 16px; font-weight:bold;">ALL about colourlove5k fan page.</strong>
        <br/>
        <br/>
        <fb:like href="https://www.facebook.com/colourlove5k" layout="button_count" action="like" show_faces="true" share="true"></fb:like>
        <br/>
        <br/>
        <fb:like-box href="http://www.facebook.com/colourlove5k" width="350" colorscheme="light" show_faces="true" header="true" stream="true" show_border="true"></fb:like-box>
      </div>
      <div id="page">
            <div id="slot" class="slot-wrapper">
                <section class="top-box">
                    <div id="display">
                        <div id="credits">0</div>
                        <div id="win">0</div>
                    </div>
                    <div class="side top"></div>
                    <div class="side bottom"></div>
                    <div class="side left"></div>
                    <div class="side right"></div>
                    <div class="side front glas"></div>
                    <div class="side back"></div>
                    <div class="wheels">
                        <section class="wheel" id="wheel-1">
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                        </section>
                        <section class="wheel" id="wheel-2">
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                        </section>
                        <section class="wheel" id="wheel-3">
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                        </section>
                    </div>
                </section>
                <section class="bottom-box">
                    <div class="side top"></div>
                    <div class="side bottom"></div>
                    <div class="side left"></div>
                    <div class="side right"></div>
                    <div class="side front">
                        <div class="fix"></div>
                        <div id="play">
                            <div class="side top"></div>
                            <div class="side bottom"></div>
                            <div class="side left"></div>
                            <div class="side right"></div>
                            <div class="side front">spin</div>
                        </div>
                    </div>
                    <div class="side back"></div>
                </section>
                <section class="side"></section>
            </div>
        </div>      
      <pre>
        <?php //print htmlspecialchars(print_r($user_profile, true)) ?>
      </pre>
    <?php } else { ?>
      <fb:login-button perms="email,user_likes,read_friendlists,user_birthday,publish_actions,user_location,friends_birthday"></fb:login-button>
    <?php } ?>
    <div id="fb-root"></div>
    <script>
      window.fbAsyncInit = function() {
        FB.init({
          appId: '<?php echo $appID; ?>',
          cookie: true,
          xfbml: true,
          oauth: true
        });
        FB.Event.subscribe('auth.login', function(response) {
          window.location.reload();
        });
        FB.Event.subscribe('auth.logout', function(response) {
          window.location.reload();
        });
      };
      (function() {
        var e = document.createElement('script'); e.async = true;
        e.src = document.location.protocol +
          '//connect.facebook.net/en_US/all.js';
        document.getElementById('fb-root').appendChild(e);
      }());
    </script>
  </body>
</html>
