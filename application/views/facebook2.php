<!DOCTYPE html>
<html xmlns:fb="http://www.facebook.com/2008/fbml">
  <head>
        <title>Slot Machine</title>
        <link rel="stylesheet" href="<?php echo base_url('/assets/game/css/reset.css')?>">
        <link rel="stylesheet" href="<?php echo base_url('/assets/game/css/slot.css')?>">
        <script src="<?php echo base_url('/assets/game/js/jquery-1.8.1.min.js')?>"></script>
        <script src="<?php echo base_url('/assets/game/js/jquery.slots.js')?>"></script>
  </head>
  <body>
    <div id="fb-root"></div>
    <script>
      window.fbAsyncInit = function() {
        FB.init({
          appId: '<?php echo $appID; ?>',
          cookie: true,
          xfbml: true,
          //oauth: true
        });
        FB.Event.subscribe('auth.authResponseChange', function(response) 
    	{
     	 if (response.status === 'connected') 
      	{
      		document.getElementById("message").innerHTML +=  "<br>Connected to Facebook";
      		//SUCCESS
      		
      	}	 
    	else if (response.status === 'not_authorized') 
        {
        	document.getElementById("message").innerHTML +=  "<br>Failed to Connect";
    
    		//FAILED
        } else 
        {
        	document.getElementById("message").innerHTML +=  "<br>Logged Out";
    
        	//UNKNOWN ERROR
        }
    	});	
	
    };
    
   	function Login()
	{
	
		FB.login(function(response) {
		   if (response.authResponse) 
		   {
		    	getUserInfo();
  			} else 
  			{
  	    	 console.log('User cancelled login or did not fully authorize.');
   			}
		 },{scope: 'email,user_likes,read_friendlists,publish_actions,user_birthday'});
	
	
	}

  function getUserInfo() {
	    FB.api('/me', function(response) {

	  var str="<b>Name</b> : "+response.name+"<br>";
	  	  str +="<b>Link: </b>"+response.link+"<br>";
	  	  str +="<b>Username:</b> "+response.username+"<br>";
	  	  str +="<b>id: </b>"+response.id+"<br>";
	  	  str +="<b>Email:</b> "+response.email+"<br>";
	  	  str +="<input type='button' value='Get Photo' onclick='getPhoto();'/>";
	  	  str +="<input type='button' value='Logout' onclick='Logout();'/>";
	  	  document.getElementById("status").innerHTML=str;
	  	  	    
    });
    }
	function getPhoto()
	{
	  FB.api('/me/picture?type=normal', function(response) {

		  var str="<br/><b>Pic</b> : <img src='"+response.data.url+"'/>";
	  	  document.getElementById("status").innerHTML+=str;
	  	  	    
    });
	
	}
	function Logout()
	{
		FB.logout(function(){document.location.reload();});
	}

  // Load the SDK asynchronously
  (function(d){
     var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement('script'); js.id = id; js.async = true;
     js.src = "//connect.facebook.net/en_US/all.js";
     ref.parentNode.insertBefore(js, ref);
   }(document));
      
    </script>
    <?php if ($user) { ?>
      <div id="page">
            <div id="slot" class="slot-wrapper">
                <section class="top-box">
                    <div id="display">
                        <div id="credits">0</div>
                        <div id="win">0</div>
                    </div>
                    <div class="side top"></div>
                    <div class="side bottom"></div>
                    <div class="side left"></div>
                    <div class="side right"></div>
                    <div class="side front glas"></div>
                    <div class="side back"></div>
                    <div class="wheels">
                        <section class="wheel" id="wheel-1">
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                        </section>
                        <section class="wheel" id="wheel-2">
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                        </section>
                        <section class="wheel" id="wheel-3">
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                            <div class="part"></div>
                        </section>
                    </div>
                </section>
                <section class="bottom-box">
                    <div class="side top"></div>
                    <div class="side bottom"></div>
                    <div class="side left"></div>
                    <div class="side right"></div>
                    <div class="side front">
                        <div class="fix"></div>
                        <div id="play">
                            <div class="side top"></div>
                            <div class="side bottom"></div>
                            <div class="side left"></div>
                            <div class="side right"></div>
                            <div class="side front">spin</div>
                        </div>
                    </div>
                    <div class="side back"></div>
                </section>
                <section class="side"></section>
            </div>
        </div>
        <br/>
      <br>
      Your user profile is
      <pre>
        <?php print htmlspecialchars(print_r($user_profile, true)) ?>
      </pre>
    <?php } else { ?>
      <!-- <fb:login-button></fb:login-button> -->
      <div id="status">
        Click on Below Image to start the demo: <br/>
        <img src="http://hayageek.com/examples/oauth/facebook/oauth-javascript/LoginWithFacebook.png" style="cursor:pointer;" onclick="Login()"/>
      </div>
        <br/>
      <div id="message">
        Logs:<br/>
      </div>
    <?php } ?>
  </body>
</html>
